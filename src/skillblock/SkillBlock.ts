// TypeScript file
// 技能块
class SkillBlock extends egret.DisplayObjectContainer {

    // 技能图标
    private skillTexture:egret.Texture;
    // 技能类型
    private skillType:number;
    // 展示资源
    private bitmapIcon:egret.Bitmap;
    // UID
    private id:number;
    // 连续的第几个技能块
    private continuityIndex:number;

    public constructor(texture:egret.Texture, type:number) {
        super();
        this.skillTexture = texture;
        this.skillType = type;
        this.bitmapIcon = new egret.Bitmap(texture);
        this.bitmapIcon.scaleX = 0.5;
        this.bitmapIcon.scaleY = 0.5;
        this.addChild(this.bitmapIcon);
        this.id = -1;
        this.continuityIndex = 0;
    }

    public getSkillType() {
        return this.skillType;
    }

    public setId(newId:number) {
        this.id = newId;
    }

    public getId() {
        return this.id;
    }

    public setContinuityIndex(newContinuityIndex:number) {
        this.continuityIndex = newContinuityIndex;
    }

    public getContinuityIndex() {
        return this.continuityIndex;
    }
}