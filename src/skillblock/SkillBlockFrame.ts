// TypeScript file
// 消除与生成冲突的bug待修复
class SkillBlockFrame extends egret.DisplayObjectContainer {

    // 技能框图片
    private frame:egret.Shape;
    // 产生技能的计时器
    private skillProduceTimer:egret.Timer;
    // 技能对象池
    private skillBlockCache:Object = {};
    // 技能图片数组
    private skillBlockArray:SkillBlock[] = [];
    // 技能产生的时间间隔单位ms，默认1000ms
    private interval:number = 1000;
    // 技能类型列表
    private skillTypeArray:number[] = [1, 2, 3]; 
    // 禁止频繁操作标志
    private isProcessing:boolean = false;

    private SKILL_BLOCK_WIDTH:number = 80;
    private MAX_BLOCK_NUM:number = 7;
    private SLIDING_TIME:number = 200;
    private FRAME_WIDTH:number = 600;
    private FRAME_HEIGHT:number = 90;
    private FRAME_MARGIN_HORIZONTAL:number = 100;

    public constructor(interval:number) {
        super();
        this.interval = interval;
        this.frame = new egret.Shape();
        this.frame.graphics.lineStyle(20, 0x0000ff);
        this.frame.graphics.beginFill(0x000000, 0);
        this.frame.graphics.drawRect(0, 0, this.FRAME_WIDTH, this.FRAME_HEIGHT);
        this.frame.graphics.endFill;
        this.addChild(this.frame);

        this.skillProduceTimer = new egret.Timer(this.interval);
        this.skillProduceTimer.addEventListener(egret.TimerEvent.TIMER, this.addSkillBlockToFrame, this);
    }

    // 从资源池中拿出技能对象
    private produceSkillBlock(skillType:number) {
        if(this.skillBlockCache[skillType]==null)
            this.skillBlockCache[skillType] = [];
        var dict:SkillBlock[] = this.skillBlockCache[skillType];
        var skillBlock:SkillBlock;
        if(dict.length>0) {
            skillBlock = dict.pop();
        } else {
            skillBlock = new SkillBlock(RES.getRes(contants.SkillImageResource.SKILL_IMAGE_ARRAY[skillType]), skillType);
        }
        skillBlock.setId(new Date().getTime());
        skillBlock.setContinuityIndex(0);
        return skillBlock;
    }

    // 回收技能对象并放入资源池
    private reclaim(skillBlock:SkillBlock) {
        var skillType:number = skillBlock.getSkillType();
        if(this.skillBlockCache[skillType]==null)
            this.skillBlockCache[skillType] = [];
        var dict:SkillBlock[] = this.skillBlockCache[skillType];
        if(dict.indexOf(skillBlock)==-1)
            dict.push(skillBlock);
    }

    // 添加技能到界面
    private addSkillBlockToFrame() {
        let skillBlockArrayLength:number = this.skillBlockArray.length;
        let that = this;
        if(skillBlockArrayLength >= this.MAX_BLOCK_NUM) {
            return;
        } else {
            let skillTpye:number = Math.ceil(Math.random()*this.skillTypeArray.length);
            let skillBlock:SkillBlock = this.produceSkillBlock(skillTpye);
            this.addChild(skillBlock);
            egret.Tween.get(skillBlock).to({
                x:this.FRAME_WIDTH - this.FRAME_MARGIN_HORIZONTAL - skillBlockArrayLength * this.SKILL_BLOCK_WIDTH},
                this.SLIDING_TIME * (this.MAX_BLOCK_NUM-skillBlockArrayLength) / this.MAX_BLOCK_NUM)
                .call(function(){
                    that.skillBlockArray[skillBlockArrayLength] = skillBlock;
                    that.continuityCheck(skillBlockArrayLength);
                    skillBlock.touchEnabled = true;
                    skillBlock.addEventListener(egret.TouchEvent.TOUCH_TAP, that.eraseCheck, that, true);
                });
        }
    }

    // 开始生产技能
    public startProduceSkill() {
        this.skillProduceTimer.start();
    }

    // 停止生产技能
    public stopProduceSkill() {
        this.skillProduceTimer.stop();
    }

    // 连续检测
    private continuityCheck(index:number) {
        let left:number = index - 1;

        if(left < 0 || this.skillBlockArray[left] == undefined || this.skillBlockArray[index] == undefined) {
            return;
        }

        let chosenSkillType:number = this.skillBlockArray[index].getSkillType();
        // 左边技能和当前技能一样
        if(this.skillBlockArray[left].getSkillType() == chosenSkillType) {  
            if(this.skillBlockArray[left].getContinuityIndex() < 2) {
                this.skillBlockArray[index].setId(this.skillBlockArray[left].getId());
                this.skillBlockArray[index].setContinuityIndex(this.skillBlockArray[left].getContinuityIndex() + 1);
            }
        }
    }

    // 消除检测
    private eraseCheck(evt:egret.TouchEvent) {
        // 防止频繁操作
        if(this.isProcessing == true) {
            return;
        } 
        // 设置操作中标志
        this.isProcessing = true;

        let index:number = 1;
        let left:number = index - 1;
        let right:number = index + 1;
        let currentId = this.skillBlockArray[index].getId();
        let skillBlockLength = this.skillBlockArray.length;
        let that = this;

        // 检测左侧
        while(left >= 0 && this.skillBlockArray[left].getId() == currentId) {
            left = left - 1;
        }

        // 检测右侧
        while(right < skillBlockLength && this.skillBlockArray[right].getId() == currentId) {
            right = right + 1;
        }

        left = left + 1;
        right = right - 1;
        let eraseLength:number = right - left + 1;
        console.log("left:" + left + " right:" + right + " eraseLength:" + eraseLength);
 
        // 消除技能块
        for(let i:number = left; i <= right; i++) {
            let skillBlock:SkillBlock = this.skillBlockArray[i];
            skillBlock.touchEnabled = false;
            skillBlock.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.eraseCheck, this, true);
            this.removeChild(skillBlock);
            this.reclaim(skillBlock);
        }

        // 更新数据
        if(left == 0) {
            if(right >= skillBlockLength - 1) {
                this.skillBlockArray = [];
            } else {
                this.skillBlockArray = this.skillBlockArray.slice(right + 1, skillBlockLength);
            }
        } else {
            if (right >= skillBlockLength - 1) {
                this.skillBlockArray = this.skillBlockArray.slice(0, left);
            } else {
                this.skillBlockArray = this.skillBlockArray.slice(0, left).concat(this.skillBlockArray.slice(right+1, skillBlockLength));
            }
        }

        // 更新数组长度
        skillBlockLength = this.skillBlockArray.length;
        console.log("skillBlockLength:" + skillBlockLength);

        // 连续检测
        for(let i:number = left; i < skillBlockLength; i++) {
            this.continuityCheck(i);
        }

        // 移动位置
        let tweenToIndex = left;
        for(let i:number = left; i < skillBlockLength; i++) {
            let skillBlock:SkillBlock = this.skillBlockArray[i];
            egret.Tween.get(skillBlock).to({
                x:this.FRAME_WIDTH - this.FRAME_MARGIN_HORIZONTAL - tweenToIndex * this.SKILL_BLOCK_WIDTH},
                150);
            tweenToIndex++;
        }

        // 解除操作中标志
        this.isProcessing = false;
    }
}
