module contants{
	export class SkillImageResource {
		public static SKILL_IMAGE_ARRAY:{[key:number]:string} = {
			1:"skillIcon1",
			2:"skillIcon2",
			3:"skillIcon3",
			4:"skillIcon4",
			5:"skillIcon5",
			6:"skillIcon6"
		};
	}
}