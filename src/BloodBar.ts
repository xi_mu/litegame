class BloodBar extends egret.DisplayObjectContainer{
    private bloodBar:egret.Sprite;
    private bloodBarBorder:egret.Sprite;
    private bloodNumber:number = 100;
    private bloodBarMargin:number = 20;

    public constructor(){
        super();
        this.initBloodBar();
    }

    private initBloodBar(){
        // this.bloodBarBorder = new egret.Sprite();
        // this.bloodBarBorder.graphics.lineStyle( 10, 0x00ff00 );
        // this.bloodBarBorder.graphics.beginFill( 0xffaa0000 );
        // this.bloodBarBorder.graphics.drawRect(0, 0, 105, 25);
        // this.bloodBarBorder.graphics.endFill();
        // this.addChild(this.bloodBarBorder);

        this.bloodBar = new egret.Sprite();
        this.bloodBar.graphics.beginFill( 0xaa0000 );
        this.bloodBar.graphics.drawRect(5, 5, 100, 20);
        this.bloodBar.graphics.endFill();
        this.addChild(this.bloodBar);
    }

    public setPosition(x:number, y:number){
        this.x = x;
        this.y = y;
    }

    public underAttack(hurt:number){
        this.bloodNumber = this.bloodNumber - hurt;
        if(this.bloodNumber < 0){
            this.bloodNumber = 0;
        }
        this.bloodBar.graphics.clear();
        this.bloodBar.graphics.beginFill( 0xaa0000 );
        this.bloodBar.graphics.drawRect(0, 0, this.bloodNumber, 20);
        this.bloodBar.graphics.endFill();
    }
}